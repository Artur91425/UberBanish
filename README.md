# UberBanish
This is a rewritten version of the 2006 addon. Changed the mechanics of addon work and fixed some bugs.  
**THIS IS A TEST VERSION OF THE ADDON, POSSIBLE SMALL BUGS!**

## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/UberBanish/-/archive/master/UberBanish-master.zip)**
2. Unpack the Zip file
3. Rename the folder "UberBanish-master" to "UberBanish"
4. Copy "UberBanish" into Wow-Directory\Interface\AddOns\
5. Restart WoW

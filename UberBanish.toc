## Interface: 11200
## Title: UberBanish
## Notes: Automatically counts down your banish timer into raid or party chat, notifies other warlocks of early breaks or banisher deaths.
## Notes-ruRU: Автоматически подсчитывает таймер изгнания чат рейда или группы, уведомляет других чернокнижников о ранних прерываниях или о смерти других чернокнижников во время изгнания.
## Author: Artur91425
## Version: 2.1
## SavedVariables: UberBanishDB

## X-Date: 29.03.2019
## X-Credits: Original author Aule (joeh@foldingrain.com)
## X-Website: https://vk.com/id65515748
## X-Category: Warlock 
## X-Commands: /uberbanish, /ub

init.lua

Locale\Localization.lua
Locale\Localization_ruRU.lua
Locale\Localization_other.lua

frames.lua
core.lua
